from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    return render(request,"story3.html")

def family(request):
    return render(request,"story3_2.html")

def reg(request):
    return render(request,"story3_3.html")

# Create your views here.
