from django.urls import path

from Story4 import views

urlpatterns = [
    path('', views.home, name='home'),
    path('family', views.family, name="family"),
    path('reg', views. reg, name="reg"),
]